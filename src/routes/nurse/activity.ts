import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import _ from 'lodash';
import { DateTime } from 'luxon';


import { ActivityModel } from '../../models/nurse/activity';
import addSchema from '../../schema/nurse/add_nurse_note';


export default async (fastify: FastifyInstance, _options: any, done: any) => {

    const db = fastify.db;
    const activityModel = new ActivityModel


    // Create By RENFIX insert nurse_note
    fastify.post('/nurse/note', {
        preHandler: [fastify.guard.role('admin')],
        schema: addSchema,
    }, async (request: FastifyRequest, reply: FastifyReply) => {
        try {
            const body: any = request.body;
            const {
                nurse_note_date,
                nurse_note_time,
                problem_list, //เป็น Json
                activity, //เป็น Json
                evaluate, //เป็น Json
                admit_id
            } = body;

            const now = DateTime.now().setZone('Asia/Bangkok');
           
            const userId = request.user.sub;


            const data: any = {
                'nurse_note_date': nurse_note_date,
                'nurse_note_time': nurse_note_time,
                'problem_list': problem_list, //เป็น Json
                'activity': activity, //เป็น Json
                'evaluate': evaluate, //เป็น Json
                'create_date': now,
                'create_by': userId,
                'modify_date': now,
                'modify_by': userId,
                'admit_id':admit_id


            }

            await activityModel.insertNurseNote(db, data);
            return reply.status(StatusCodes.OK)
                .send({ status: 'ok' });
        } catch (error: any) {
            request.log.error(error);
            return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
                .send({
                    status: 'error',
                    error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
                });
        }
    });

    done();
}