import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import _ from 'lodash';
import { DateTime } from 'luxon';

import { VitalSignModel } from '../../models/nurse/vital_sign';
import add_vital_sign from '../../schema/nurse/add_vital_sign';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

    const db = fastify.db;
    const vitalSignModel = new VitalSignModel();

    // add VitalSign Create by RENFIX
    fastify.post('/', {
        preHandler: [
            fastify.guard.role(['admin','nurse','doctor'])
            // fastify.guard.scope(['nurse.ward','nurse.create'])
          ],
        schema: add_vital_sign
    }, async (request: FastifyRequest, reply: FastifyReply) => {
        try {

            const body: any = request.body;
            const { //id, ไม่เอาเนื่องจาก database จะ genมาให้เป็น uuid
                admit_id,
                vital_sign_date,
                vital_sign_time,
                body_temperature,
                pulse_rate,
                respiratory_rate,
                systolic_blood_pressure,
                diatolic_blood_pressure,
                intake_oral_fluid,
                intake_penterate,
                intake_medicine,
                outtake_urine,
                outtake_emesis,
                outtake_drainage,
                outtake_aspiration,
                outtake_lochia,
                stools,
                urine,
                pain_score,
                oxygen_sat,
                body_weight,
               // create_at,
                create_by,
               // modify_at,
                modify_by

            } = body;

            const signTimeStampNow = DateTime.now().setZone('Asia/Bangkok');
            const signDateNow = DateTime.now().toSQL();
            const signTimeNow = DateTime.now().toFormat('HH:mm:ss');
            const data: any = {
               // 'id': id, ไม่เอาเนื่องจาก database จะ genมาให้เป็น uuid
                'admit_id': admit_id,
                'vital_sign_date': signDateNow,
                'vital_sign_time': signTimeNow,
                'body_temperature': body_temperature,
                'pulse_rate': pulse_rate,
                'respiratory_rate': respiratory_rate,
                'systolic_blood_pressure': systolic_blood_pressure,
                'diatolic_blood_pressure': diatolic_blood_pressure,
                'intake_oral_fluid': intake_oral_fluid,
                'intake_penterate': intake_penterate,
                'intake_medicine': intake_medicine,
                'outtake_urine': outtake_urine,
                'outtake_emesis': outtake_emesis,
                'outtake_drainage': outtake_drainage,
                'outtake_aspiration': outtake_aspiration,
                'outtake_lochia': outtake_lochia,
                'stools': stools,
                'urine': urine,
                'pain_score': pain_score,
                'oxygen_sat': oxygen_sat,
                'body_weight': body_weight,
                'create_at': signTimeStampNow,
                'create_by': create_by,
                'modify_at': signTimeStampNow,
                'modify_by': modify_by




            }

            await vitalSignModel.insertVitalSign(db, data);
            return reply.status(StatusCodes.OK)
                .send({ status: 'ok' });
        } catch (error: any) {
            request.log.error(error);
            return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
                .send({
                    status: 'error',
                    error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
                });
        }
    });




    done();

}

