import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';

import { UUID } from 'crypto';

import { AdmitService } from '../../models/nurse/admit';
import createSchema from '../../schema/nurse/create';
import updateSchema from '../../schema/nurse/update';
import listSchema from '../../schema/nurse/list';
import { lstatSync } from 'fs';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const admitService = new AdmitService();

  fastify.post('/', {
    preHandler: [
      fastify.guard.role(['nurse', 'admin','doctor']),
      // fastify.guard.scope('nurse.create', 'admit.create', 'nurse.read', 'admit.read')
    ],
    schema: createSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {

      // jwt decoded
      const userId: any = request.user.sub;

      const body: any = request.body;
      const {
        an,
        hn,
        vn,
        insurance_id,
        department_id,
        pre_diag,
        ward_id,
        bed_id,
        admit_date,
        admit_time,
        doctor_id,
        admit_by
      } = body;

      const data: any = {
        an,
        hn,
        vn,
        insurance_id,
        department_id,
        pre_diag,
        ward_id,
        bed_id,
        admit_date,
        admit_time,
        doctor_id,
        admit_by,
        create_by: userId
      }

      await admitService.save(db, data);

      return reply.status(StatusCodes.CREATED).send({ ok: true });

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })

  fastify.put('/:id', {
    preHandler: [
      fastify.guard.role(['nurse', 'admin','doctor']),
      // fastify.guard.scope('nurse.create', 'admit.create', 'nurse.read', 'admit.read')
    ],
    schema: updateSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {

      const req: any = request
      const data: any = req.body
      const id: any = req.params.id

      await admitService.update(db, id, data)
      return reply.status(StatusCodes.OK).send({ ok: true });

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error);
    }
  })

  fastify.delete('/:id', {
    preHandler: [
      fastify.guard.role(['nurse', 'admin','doctor']),
      // fastify.guard.scope('nurse.create', 'admit.create', 'nurse.read', 'admit.read')
    ],
    // schema: updateSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {

      const req: any = request
      const id: any = req.params.id

      await admitService.delete(db, id)
      return reply.status(StatusCodes.OK).send({ ok: true });

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error);
    }
  })

  fastify.get('/', {
    preHandler: [
      fastify.guard.role('nurse')],
    // fastify.guard.scope('nurse.ward')],
    schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const req: any = request.query
      const ward_id: any = req.ward_id
      const query = req.query
      const limit: any = req.limit
      const offset: any = req.offset

      const results: any = await admitService.infoActive(db, ward_id, query, limit, offset);

      // total
      const resultTotal: any = await admitService.infoActiveTotal(db, ward_id, query)

      return reply.status(StatusCodes.OK)
        .send({
          ok: true,
          data: results,
          total: Number(resultTotal[0].total)
        });

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error);
    }
  })

  fastify.get('/:id', {
    preHandler: [
      fastify.guard.role(['admin', 'nurse', 'doctor'])
      // fastify.guard.scope(['nurse.ward','nurse.create'])
    ],
    // schema: listSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const req: any = request.params
      const id: any = req.id

      const data: any = await admitService.infoByAdmitID(db, id)

      return reply.status(StatusCodes.OK)
        .send({
          ok: true,
          data,
        });

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send(error);
    }
  })



  fastify.post('/upsert-admit', {
    preHandler: [fastify.guard.role('admin')], //เช็คว่าเป็น Admin หรือไม่ 

  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {

      const body: any = request.body;
      const data: any = { ...body };// รับ body ทั้งหมดที่ frontend ส่งมา
      let dataAdmit: any = data['admit'];// ดึงเอาเฉพาะ table admit
      let dataPatient: any = data['patient'];// ดึงเอาเฉพาะ table patient
      let dataOpdReview: any = data['review'];// ดึงเอาเฉพาะ table review
      let dataTreatment: any = data['treatment'];
      let dataBed: any = data['bed'];
      let dataWardID: any = data['ward'];
      let dataDepartmentID: any = data['department'];// ดึงเอาเฉพาะ table review
      console.log('sssssss', dataDepartmentID);
      //console.log('ดึงเฉพาะ patienr',dataPatient);

      let admitAN: any = dataAdmit[0].an; // ดึงเอาเฉพาะ an ใน table admit
      let patientAN: any = dataPatient[0].an;
      let OpdReviewAN: any = dataOpdReview[0].an;
      let treatmentAN: any = dataTreatment[0].an;
      //let itemAdmit: any;


      let rsAdmit: any = await admitService.viewIDByAN(db, admitAN);

      //console.log('ข้อมูลUUID ที่ได้', rsAdmit[0].id);

      //let admit_UUID = rsAdmit[0].id;

      if (rsAdmit[0]) {
        if (!dataOpdReview[0].eye_score) {
          delete dataOpdReview[0].eye_score;
          dataOpdReview[0].eye_score = null;
        }
        if (!dataOpdReview[0].movement_score) {
          delete dataOpdReview[0].movement_score;
          dataOpdReview[0].movement_score = null;
        }
        if (!dataOpdReview[0].verbal_score) {
          delete dataOpdReview[0].verbal_score;
          dataOpdReview[0].verbal_score = null;
        }

        let rsInscl: any = dataAdmit[0].inscl_code;

        delete dataAdmit[0].inscl_code;
        delete dataAdmit[0].ward_code;
        delete dataAdmit[0].bed_code;
        delete dataAdmit[0].department_code;




        dataAdmit[0].ward_id = dataWardID[0].ward_id;
        dataAdmit[0].bed_id = dataBed[0].bed_id;
        dataAdmit[0].department_id = dataDepartmentID[0].department_id;
        dataAdmit[0].inscl = rsInscl;


        await admitService.updateAdmitByAN(db, dataAdmit[0], admitAN);
        await admitService.updatePartient(db, dataPatient[0], patientAN);
        await admitService.updateOpdReview(db, dataOpdReview[0], OpdReviewAN);
        await admitService.updateTreatement(db, dataTreatment[0], treatmentAN);



      } else {

        let rsDep = await admitService.selectDepartment(db, dataAdmit[0].department_code)
        let rsInscl: any = dataAdmit[0].inscl_code;

        delete dataAdmit[0].inscl_code;

        delete dataAdmit[0].ward_code;
        delete dataAdmit[0].bed_code;
        delete dataAdmit[0].department_code;

        dataAdmit[0].ward_id = dataWardID[0].ward_id;
        dataAdmit[0].bed_id = dataBed[0].bed_id;
        dataAdmit[0].inscl = rsInscl;

        dataAdmit[0].department_id = dataDepartmentID[0].department_id;

        await admitService.insertAdmit(db, dataAdmit);


        let rsAdmit: any = await admitService.viewIDByAN(db, admitAN);
        // console.log('ข้อมูลUUID ที่ได้', rsAdmit[0].id);

        let admit_UUID = rsAdmit[0].id;

        dataPatient[0].admit_id = rsAdmit[0].id; //แทรกฟิว admit_id ลงไปในคิวรี่

        dataOpdReview[0].admit_id = rsAdmit[0].id;


        for (let i of dataTreatment) {
          i.admit_id = rsAdmit[0].id;
        } //แทรกฟิว admit_id ลงไปในคิวรี่
        console.log("for check ", dataTreatment);

        await admitService.insertPatient(db, dataPatient);
        await admitService.insertOpdReview(db, dataOpdReview);
        await admitService.insertTreatement(db, dataTreatment);


      }
      // console.log(rs);

      // return reply.status(StatusCodes.OK)
      //  .send({ status: 'ok', data1: rs, mst: '--------------' });

      return reply.status(StatusCodes.OK)
        .send({ ok: true, rsAdmit });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: 'error',
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  });

  done();

}
