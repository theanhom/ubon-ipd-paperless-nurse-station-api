import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';
import { AdmitService } from '../../models/admit';

// schema
import createSchema from '../../schema/admit/create';
import { HttpStatusCode } from 'axios';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const admitService = new AdmitService();


  fastify.post('/create', {
    preHandler: [
      fastify.guard.role('admin'),
      fastify.guard.scope('order.create')],
    schema: createSchema,
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {

      // jwt decoded
      const userId: any = request.user.sub;

      const body: any = request.body;
      const {
        an,
        hn,
        vn,
        insurance_id,
        department_id,
        pre_diag,
        ward_id,
        bed_id,
        admit_date,
        admit_time,
        doctor_id,
        admit_by
      } = body;

      const data: any = {
        an,
        hn,
        vn,
        insurance_id,
        department_id,
        pre_diag,
        ward_id,
        bed_id,
        admit_date,
        admit_time,
        doctor_id,
        admit_by,
        create_by: userId
      }

      await admitService.create(db, data);

      return reply.status(StatusCodes.CREATED).send({ ok: true });

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })


 



  done();

} 
