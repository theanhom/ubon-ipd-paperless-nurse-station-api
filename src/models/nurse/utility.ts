import { Knex } from 'knex';
export class UtilityModel {

  //Create by RENFIX
  // transection เปลี่ยน ward/ update ตาราง admit เสร็จแล้วไป insert ที่ตารางpatient_move ต่อ
  changeWardTransaction(db: Knex, id: any, data: any, data2: any) {
    return new Promise((resolve: any, reject: any) => {
      db.transaction((trx) => {
        db('admit')
          .update(data)
          .where('id', id)
          .transacting(trx)
          .then(() => {
            // insert
            return db('patient_move')
              .insert(data2)
              .then(trx.commit)
              .catch(trx.rollback);
          })

      }).then(() => {
        resolve();
      })
        .catch((error: any) => {
          reject(error);
        })
    })
  }

  // Create By RENFIX
  // transection เปลี่ยนเตียง  update ตาราง admit เสร็จแล้วไป insert ที่ตาราง partient_move ต่อ
  assignBedTypeTransaction(db: Knex, id: any, data: any, data2: any) {
    return new Promise((resolve: any, reject: any) => {
      db.transaction((trx) => {
        db('admit')
          .update(data)
          .where('id', id)
          .transacting(trx)
          .then(() => {
            // insert
            return db('patient_move')
              .insert(data2)
              .then(trx.commit)
              .catch(trx.rollback);
          })

      }).then(() => {
        resolve();
      })
        .catch((error: any) => {
          reject(error);
        })
    })
  }

}
