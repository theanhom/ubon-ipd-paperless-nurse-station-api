import { UUID } from 'crypto';
import { Knex } from 'knex';
export class InfoModel {
  info(db: Knex, id:UUID) {
    let sql = db('admit as a')
      .leftJoin('patient as p', 'a.an','p.an')
      .leftJoin('opd_review as o', 'a.id','o.admit_id')
      .select(
        'p.title', 'p.fname', 'p.lname','p.gender', 
        'p.age', 'p.address', 'p.phone','p.inscl_name',
        'o.chief_compaint', 'o.present_illness', 'o.past_history', 'o.physical_exam', 
        'o.body_temperature', 'o.body_weight','o.body_height', 'o.waist', 'o.pulse_rate', 'o.respiratory_rate', 
        'o.systolic_blood_pressure', 'o.diatolic_blood_pressure', 
        'o.oxygen_sat', 'o.eye_score', 'o.movement_score', 'o.verbal_score'
      )
      .where('a.id', id);
      return sql
  }
}