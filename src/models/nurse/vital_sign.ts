import { Knex } from 'knex';

export class VitalSignModel {
    
    // Create by RENFIX
    insertVitalSign(db: Knex, data: any) {
        return db('vital_sign')
          .insert(data);
      }

}