import { UUID } from 'crypto';
import { Knex } from 'knex';
import patient from '../../routes/patient';

export class AdmitService {
  /**
   * บันทึกข้อมูล Admit
   * @param db 
   * @param data 
   * @returns 
   */
  save(db: Knex, data: object) {
    return db('admit')
      .insert(data);
  }

  /**
   * 
   * @param db 
   * @param id 
   * @param data 
   * @returns 
   */
  update(db: Knex, id: UUID, data: object) {
    let sql = db('admit')
      .update(data)
      .where('id', id)
    return sql
  }

  /**
   * 
   * @param db 
   * @param id 
   * @returns
   */
  delete(db: Knex, id: UUID) {
    let sql = db('admit')
      .delete()
      .where('id', id)
    return sql
  }

  /**
   * 
   * @param db 
   * @param wardId
   * @param query 
   * @param limit 
   * @param offset 
   * @returns 
   */
  infoActive(db: Knex, wardId: UUID, query: string, limit: number, offset: number) {
    let sql = db('admit as a')
      .select('a.*', 'p.cid', 'p.fname', 'p.lname', 'p.gender', 'p.age')
      .innerJoin('patient as p', 'p.an', 'a.an')

    if (wardId) {
      sql.where(builder => {
        builder.whereRaw('a.ward_id = ?', [wardId])
      })
    }

    if (query) {
      let _query = `%${query}%`
      sql.where(builder => {
        builder.orWhere('p.cid', query)
          .orWhere('a.hn', query)
          .orWhereILike('p.fname', _query)
          .orWhereILike('p.lname', _query)
      })
    }

    return sql
      .where('a.is_active', true)
      .limit(limit).offset(offset);

  }

  infoActiveTotal(db: Knex, wardId: UUID, query: string) {
    let sql = db('admit as a')
      .innerJoin('patient as p', 'p.an', 'a.an')

    if (wardId) {
      sql.where(builder => {
        builder.whereRaw('a.ward_id = ?', [wardId])
      })
    }

    if (query) {
      let _query = `%${query}%`
      sql.where(builder => {
        builder.orWhere('p.cid', query)
          .orWhere('a.hn', query)
          .orWhereILike('p.fname', _query)
          .orWhereILike('p.lname', _query)
      })
    }

    return sql
      .where('a.is_active', true)
      .count({ total: '*' });

  }



  // Create By RENFIX การใช้ Transaction สำหรับบันทึกข้อมูลแบบ Array
  //viewIDByAN เอาไว้ดึง UUID ADMIT 
  viewIDByAN(db: Knex, admitAN: any) {
    return db('admit')
      .select('id') // UUID
      .where('an', admitAN);
  }

  updateAdmitByAN(db: Knex, dataAdmit: any, admitAN: any) {
    return db('admit')
      .update(dataAdmit)
      .where('an', admitAN);
  }

  insertAdmit(db: Knex, dataAdmit: any) {
    return db('admit')
      .insert(dataAdmit)
      .returning('*');
  }

  insertPatient(db: Knex, dataPartient: any) {
    return db('patient')
      .insert(dataPartient)
  }

  updatePartient(db: Knex, dataPatient: any, patientAN: any) {
    return db('patient')
      .update(dataPatient)
      .where('an', patientAN);
  }

  insertOpdReview(db: Knex, dataOpdReview: any) {
    return db('opd_review')
      .insert(dataOpdReview)
  }

  updateOpdReview(db: Knex, dataOpdReview: any, OpdReviewAN: any) {
    return db('opd_review')
      .update(dataOpdReview)
      .where('an', OpdReviewAN);
  }

  insertTreatement(db: Knex, dataTreatment: any) {
    return db('opd_treatment')
      .insert(dataTreatment)
  }

  updateTreatement(db: Knex, dataTreatment: any, treatmentAN: any) {
    return db('opd_treatment')
      .update(dataTreatment)
      .where('an', treatmentAN);
  }

  selectDepartment(db: Knex, department_code: string) {
    return db('h_department')
      .where('code', department_code)

  }













  // AddAdmit(db: Knex, data1: any) {
  //   return new Promise((resolve: any, reject: any) => {
  //     db.transaction((trx) => {
  //       db('admit')
  //         .insert(data1)
  //         .returning('id')
  //         .onConflict(['an'])
  //         .merge()
  //         .transacting(trx)
  //         .then(trx.commit)
  //         .catch(trx.rollback);
  //     }).then(() => {
  //       resolve();
  //     })
  //       .catch((error: any) => {
  //         reject(error);
  //       });
  //   });
  // }


  infoByAdmitID(db: Knex, id: UUID) {
    let sql = db('admit as a')
      .leftJoin('patient as p', 'a.an', 'p.an')
      .leftJoin('opd_review as o', 'a.id', 'o.admit_id')
      .select(
        'p.title', 'p.fname', 'p.lname', 'p.gender',
        'p.age', 'p.address', 'p.phone', 'p.inscl_name',
        'o.chief_compaint', 'o.present_illness', 'o.past_history', 'o.physical_exam',
        'o.body_temperature', 'o.body_weight', 'o.body_height', 'o.waist', 'o.pulse_rate', 'o.respiratory_rate',
        'o.systolic_blood_pressure', 'o.diatolic_blood_pressure',
        'o.oxygen_sat', 'o.eye_score', 'o.movement_score', 'o.verbal_score'
      )
      .where('a.id', id);
    return sql
  }




































}