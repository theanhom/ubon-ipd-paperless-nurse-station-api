import { AxiosInstance } from "axios";
export class HISService {
  getWaitingList(axios: AxiosInstance, token: any, an: any = '') {
    const url: any = `/services/patient?an=${an}`;
    return axios.get(url, {
      headers: {
        'Authorization': 'Bearer ' + token
      }
    });
  }

  getWaitingListReview(axios: AxiosInstance, token: any, an: any = '') {
    const url: any = `/services/review?an=${an}`;
    return axios.get(url, {
      headers: {
        'Authorization': 'Bearer ' + token
      }
    });
  }

  getWaitingListTreatement(axios: AxiosInstance, token: any, an: any = '') {
    const url: any = `/services/treatement?an=${an}`;
    return axios.get(url, {
      headers: {
        'Authorization': 'Bearer ' + token
      }
    });
  }  

  getWaitingListAdmit(axios: AxiosInstance, token: any, an: any = '') {
    const url: any = `/services/admit?an=${an}`;
    return axios.get(url, {
      headers: {
        'Authorization': 'Bearer ' + token
      }
    });
  }  
}